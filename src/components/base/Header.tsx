import React, { useState } from 'react';
import {Toolbar} from '@material-ui/core'
export interface HeaderProps {}

const Header: React.FC<HeaderProps> = ({}) => {
  return <header>
    <Toolbar/>
  </header>;
};

export default Header;
