import React from 'react';
import './App.css';
import Header from '../components/base/Header';
import HomePage from '../pages/home/HomePage';
import ProjectMainPage from '../pages/project/ProjectMainPage';

function App() {
  return (
    <div className="App">
      <Header />
      <HomePage />
      <ProjectMainPage/>
    </div>
  );
}

export default App;
